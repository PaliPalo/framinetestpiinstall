#!/bin/bash

# Manage script arguments.
#
# 1rst -> server name (default: FraminetestEdu)
# 2nd  -> minetest user name (default : minetest)

[[ -z "$1" ]] && server_name=FraminetestEdu || server_name=$1

[[ -z "$2" ]] && minetest_user=minetest || minetest_user=$2

# Create minetest user and its home directory

sudo useradd -mU ${minetest_user}

# Create .minetest/textures and .minetest/worlds folders under minetest home directory

sudo mkdir -p /home/${minetest_user}/.minetest/textures /home/${minetest_user}/.minetest/worlds

# Change the owner of those sub-folders because root is the proprietary at the moment

sudo chown -R ${minetest_user}: /home/${minetest_user}/.minetest/

# Update repository informations (sometimes useful to update mirrors list)

sudo apt-get update

# Install the minetest-server version from the default Raspbian repository

sudo apt-get install minetest-server

# Now, the minetest server is already activated.
# We can connect to it with a minetest client.
#
# Be aware that the minetest-server version is 0.4.14 (at the 1rst July 2019)
# We must use a compatible client version:
#   - the 0.4.17 is compatible
#   - the 5.0 is not

# Go to minestest home directory

cd /home/${minetest_user}

# And updload worlds and textures, plus a template configuration file from framinetest.org

sudo wget https://framinetest.org/dl/worldmods.tar.gz https://framinetest.org/dl/textures.tar.gz https://framinetest.org/dl/minetest.conf

# Go to textures sub-folder

cd .minetest/textures

# Extract the previously downloaded textures archive in it

sudo tar xvf /home/${minetest_user}/textures.tar.gz

# Go to the worlds sub-folder

cd ../worlds

# And create the server subfolder within

sudo mkdir ${server_name}

# Go inside it

cd ${server_name}

# And extract previously downloaded worlds archive in it

sudo tar xvf /home/${minetest_user}/worldmods.tar.gz

# Bakcup the previously downloaded template configuration file

sudo cp /home/${minetest_user}/minetest.conf /home/${minetest_user}/minetest.conf.sav

# Change the downloaded configuration to meet our needs
# ( This method requires the result has to be in pi user home directory )

cat /home/${minetest_user}/minetest.conf | awk '/^server_name = .*/ { print "server_name = ${server_name}"; next } /^server_description = .*/ { print "server_description = My Minetest server"; next } /^server_address = .*/ { print "server_address = ${server_name}.org"; next } /^server_url = .*/ { print "server_url = http://${server_name}.org"; next } {print $0}' > ~/${server_name}.conf

# Copy it to its final location

sudo cp ${server_name}.conf /home/${minetest_user}/minetest.conf

# Make everything under minetest home directory the property of minetest
# (This because of extraction and copy done before)

sudo chown -R ${minetest_user}: /home/${minetest_user}/.minetest

# Allow minetest user to write into the log by adding him to games group

sudo adduser ${minetest_user} games

# And by giving this games group a write access to the folder /var/log/minetest

sudo chmod 775 /var/log/minetest

# and on the /var/log/minetest/minetest.log file too

sudo chmod 765 /var/log/minetest/minetest.log

# Stop the default minetest-server (the one we previously installed with apt)

sudo systemctl stop minetest-server

# Apply some patches to make everything works without problem.

sudo sed --in-place 's|\(description = S(\)|-- \1|g' /home/${minetest_user}/.minetest/worlds/${server_name}/worldmods/car_f1/init.lua

# Start our own minetest-server to make some tests

sudo /usr/games/minetestserver --config /home/${minetest_user}/minetest.conf
